# Tietokantasovelluksen esittelysivu

Yleisiä linkkejä:


* [Linkki sovellukseeni] (http://rpiiroin.users.cs.helsinki.fi/tsoha-drinkkiarkisto)
* [Linkki dokumentaatiooni](/doc/rpiiroin-tsoha-doc.pdf)

Test log in:
* UN: rekisteröitynyt
* pw: 1234

## Työn aihe

[linkki valmiiseen aiheeseen](http://advancedkittenry.github.io/suunnittelu_ja_tyoymparisto/aiheet/Drinkkiarkisto.html) 
