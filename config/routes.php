<?php

  // DrinksController routes

  $routes->get('/', function() {
    DrinksController::index();
  });

  $routes->get('/drink', function() {
    DrinksController::index();
  });

  $routes->post('/drink', function() {
    DrinksController::store();
  });

  $routes->get('/drink/new', function() {
    DrinksController::create();
  });

  $routes->get('/drink/:iddrink', function($iddrink) {
    DrinksController::show($iddrink);
  });

  $routes->get('/drink/:iddrink/edit', function($iddrink) {
    DrinksController::edit($iddrink);
  });

  $routes->post('/drink/:iddrink/edit', function($iddrink) {
    DrinksController::update($iddrink);
  });

  $routes->post('/drink/:iddrink/destroy', function($iddrink) {
    DrinksController::destroy($iddrink);
  });


  // UserController routes

  $routes->get('/login', function() {
    UserController::login();
  });

  $routes->post('/login', function() {
    UserController::handle_login();
  });

  $routes->post('/logout', function() {
    UserController::logout();
  });


  // IngredientController routes

  $routes->get('/ingredient/', function() {
    IngredientController::index();
  });

  $routes->get('/ingredient/new', function() {
    IngredientController::create();
  });

  $routes->post('/ingredient', function() {
    IngredientController::store();
  });

  $routes->get('/ingredient/:ingrid', function($ingrid) {
    IngredientController::show_new($ingrid);
  });

  // SandBox for testing purposes

  $routes->get('/hiekkalaatikko', function() {
    HelloWorldController::sandbox();
  });


