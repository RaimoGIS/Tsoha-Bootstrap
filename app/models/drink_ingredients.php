<?php

	class DrinkIngredients extends BaseModel{

		public $drink, $ingredient, $amount

		public function __construct($attributes){
			parent::__construct($attributes);

			// add validators
		}

		public function save(){
			$query = DB::connection()->prepare('INSERT INTO DrinkIngredients (drink, ingredient, amount) VALUES (:drink, :ingredient, :amount)');

			$query->execute(array('drink' => $this-> drink, 'ingredient' => $this-> ingredient, 'amount' => $this-> amount));
			$row = $query->fetch();
		}
	}