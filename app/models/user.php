<?php

class User extends BaseModel {

	public $iduser,
			$name,
			$password;

	public static function authenticate($username, $password){
		
		$query = DB::connection()->prepare('SELECT * FROM DrinkUser WHERE name = :name AND password = :password LIMIT 1');
		$query-> execute(array(
			'name' => $username,
			'password' => $password));
		$row = $query->fetch();

		if($row){
			$user = new User(array(
				'iduser' => $row['iduser'],
				'name' => $row['name'],
				'password' => $row['password']
				//'modifyright' => $row['modifyright'],
				//'adminright' => $row['adminright'],
				//'saveddrink' => $row['saveddrinks']
			));	
			return $user;

		}else{
			return null;
		}
		
	}

	public static function find($iduser){
		$query = DB::connection()->prepare('SELECT * FROM DrinkUser WHERE iduser = :iduser LIMIT 1');
		$query-> execute(array(
			'iduser' => $iduser));
		$row = $query->fetch();


		if($row){
			$user = new User(array(
				'iduser' => $row['iduser'],
				'name' => $row['name'],
				'password' => $row['password']
				//'modifyright' => $row['modifyright'],
				//'adminright' => $row['adminright'],
				//'saveddrink' => $row['saveddrinks']
			));
			//Kint::dump($user);	
			return $user;

		}else{
			return null;
		}
	}

}
