<?php

class Drink extends BaseModel{
	// attributes
	public $iddrink, $createdby, $createdat, $drinkname, $preparation, $accepted, $nonalcoholic, $drinktype, $description, $iddrinktype, $name, $ingr, $ingredient;
	// constructor
	public function __construct($attributes){
		parent::__construct($attributes);
		$this->validators = array('validate_name');
	}

	public static function all(){
		// new query using db-connection
		$query = DB::connection()->prepare('SELECT * FROM Drink AS d INNER JOIN DrinkTypes AS t ON d.DrinkType = t.idDrinkType');
		// execute query
		$query->execute();
		// get results
		$rows = $query->fetchAll();
		$drinks = array();

		foreach($rows as $row) {

			$drinks[] = new Drink(array(
				'iddrink' => $row['iddrink'],
				'createdby' => $row['createdby'],
				'createdat' => $row['createdat'],
				'drinkname' => $row['drinkname'],
				'preparation' => $row['preparation'],
				'accepted' => $row['accepted'],
				'nonalcoholic' => $row['nonalcoholic'],
				'drinktype' => $row['drinktype'],
				'description' => $row['description'],
				'iddrinktype' => $row['iddrinktype'],
				'name' => $row['name']
			));
		}
		return $drinks;
	}

	public static function find($iddrink){
		$query = DB::connection()->prepare("SELECT * FROM Drink INNER JOIN DrinkTypes  ON Drink.DrinkType = DrinkTypes.idDrinkType WHERE iddrink = :iddrink LIMIT 1");
		$query->execute(array('iddrink' => $iddrink));
		$row = $query->fetch();

		if($row){
			$drink = new Drink(array(
				'iddrink' => $row['iddrink'],
				'createdby' => $row['createdby'],
				'createdat' => $row['createdat'],
				'drinkname' => $row['drinkname'],
				'preparation' => $row['preparation'],
				'accepted' => $row['accepted'],
				'nonalcoholic' => $row['nonalcoholic'],
				'drinktype' => $row['drinktype'],
				'description' => $row['description'],
				'iddrinktype' => $row['iddrinktype'],
				'name' => $row['name']
			));

			return $drink;
		}

		return null;
	}

	public function validate_name(){
		$errors = array();
		if(!$this->validate_not_null($this->drinkname)){
			$errors[] = 'Name cannot be empty!';
		}
		if(!$this->validate_string_length($this->drinkname, 3)){
			$errors[] = 'Name must be atleast three characters long!';
		}
		return $errors;
	}

	public function save($ingredients){
		$query = DB::connection()->prepare('INSERT INTO Drink (drinkname, preparation, drinktype, description) VALUES (:drinkname, :preparation, :drinktype, :description) RETURNING iddrink');

		$query->execute(array('drinkname' => $this-> drinkname, 'preparation' => $this-> preparation, 'drinktype' => $this-> drinktype, 'description' => $this-> description));

		$row = $query->fetch();
		$this->iddrink = $row['iddrink'];

		foreach($ingredients as $ingr){
			$query = DB::connection()->prepare('INSERT INTO DrinkIngredients (drink, ingredient) VALUES (:iddrink, :ingr)');

			$query->execute(array(
				'iddrink' => $this-> iddrink,
				'ingr' => $ingr
			));
		}
	}

	
	public function update(){
		
		$query = DB::connection()->prepare('UPDATE Drink SET drinkname = :drinkname, preparation = :preparation, drinktype = :drinktype, description = :description WHERE iddrink = :iddrink');

		kint::dump($this);

		$query->execute(array(
			'iddrink' => $this->iddrink,
			'drinkname' => $this->drinkname, 
			'preparation' => $this->preparation, 
			'drinktype' => $this->drinktype, 
			'description' => $this->description));

		//Kint::dump($row);
	}
	

	public function destroy(){
		$query = DB::connection()->prepare('DELETE FROM Drink WHERE iddrink = :iddrink');

		$query->execute(array(
			'iddrink' => $this->iddrink));
	}

	public static function getIngredients($iddrink){
		$query = DB::connection()->prepare('SELECT * FROM DrinkIngredients WHERE drink = :iddrink');

		$query->execute(array('iddrink' => $iddrink));

		$rows = $query->fetchAll();
		$ingredients = array();

		foreach($rows as $row) {
			$ingredient = Ingredient::find($row['ingredient']);
			array_push($ingredients, $ingredient);
		}
		return $ingredients;
	}
}

