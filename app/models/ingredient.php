<?php

	class Ingredient extends BaseModel{
		// attributes
		public $ingrid, $name, $description;
		// constructor
		public function __construct($attributes){
			parent::__construct($attributes);
			
			// add validators
			$this->validators = array('validate_name');
		}

		public static function all(){
			// new query using db-connection
			$query = DB::connection()->prepare('SELECT * FROM Ingredient');
			// execute query
			$query->execute();
			// get results
			$rows = $query->fetchAll();
			$ingredients = array();

			// go through results row by row
			foreach($rows as $row) {
				$ingredients[] = new Ingredient(array(
					'ingrid' => $row['ingrid'],
					'name' => $row['name'],
					'description' => $row['description']
				));
			}
			//Kint::dump($ingredients);
			return $ingredients;
		}

		public static function find($ingrid){
			$query = DB::connection()->prepare("SELECT * FROM Ingredient WHERE ingrid = :ingrid");
			$query->execute(array('ingrid' => $ingrid));
			$row = $query->fetch();

			if($row){
				$ingredient = new Ingredient(array(
					'ingrid' => $row['ingrid'],
					'name' => $row['name'],
					'description' => $row['description']
				));

				return $ingredient;
			}

			return null;
		}

		public function save(){
			$query = DB::connection()->prepare('INSERT INTO Ingredient (name, description) VALUES (:name, :description) RETURNING ingrid');

			$query->execute(array('name' => $this-> name, 'description' => $this-> description));

			$row = $query->fetch();

			// not needed yet
			$this->ingrid = $row['ingrid'];
		}


		public function validate_name(){
			$errors = array();
			if(!$this->validate_not_null($this->name)){
				$errors[] = 'Name cannot be empty!';
			}
			if(!$this->validate_string_length($this->name, 3)){
				$errors[] = 'Name must be atleast three characters long!';
			}
			return $errors;
		}


	}