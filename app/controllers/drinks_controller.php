<?php

class DrinksController extends BaseController{
	public static function index() {
		//get all drinks from database
		self::check_logged_in();

		$drinks = Drink::all();
		View::make('drink/drink_list.html', array('drinks' => $drinks));
	}

	public static function show($iddrink){
		self::check_logged_in();

		$ingredients = Drink::getIngredients($iddrink);
		$drink = Drink::find($iddrink);

		View::make('drink/drink_show.html', array('drink' => $drink, 'ingredients' => $ingredients));
	}

	public static function create() {
		self::check_logged_in();
		// Get ingredients with IngredientController and send them to view

		$ingredients = IngredientController::show();
		View::make('drink/new.html', array('ingredients' => $ingredients));

	}

	public static function store(){
		self::check_logged_in();
		$params = $_POST;
		
		$attributes = array(
			'drinkname' => $params['drinkname'],
			'preparation' => $params['preparation'],
			'drinktype' => $params['drinktype'],
			'description' => $params['description']
		);
		
		$drink = new Drink($attributes);

		$errors = $drink->errors();

		if(count($errors) > 0){
			View::make('drink/new.html', array('errors' => $errors, 'attributes' => $attributes));
		}else{
			$ingredients = $params['drinkingredients'];
			$drink->save($ingredients);
			
			// get ingredients and pass them to show page

			Redirect::to('/drink/' . $drink->iddrink, array('message' => 'Drink has been added to your library!'));	
		}

	}

	public static function edit($iddrink){
		self::check_logged_in();

		$drink = Drink::find($iddrink);
		View::make('drink/edit.html', array('drink' => $drink));
	}

	public static function update($iddrink){
		self::check_logged_in();

		$params = $_POST;

		$attributes = array(
			'iddrink' => $iddrink,
			'drinkname' => $params['drinkname'],
			'preparation' => $params['preparation'],
			'drinktype' => $params['drinktype'],
			'description' => $params['description']
		);
		
		$drink = new Drink($attributes);
		$errors = $drink->errors();

		if(count($errors) > 0){
			View::make('drink/edit.html', array('errors' => $errors, 'attributes' => $attributes));
		}else{
			$drink->update();

			Redirect::to('/drink/' . $drink->iddrink, array('message' => 'Drink has been modified successfully!'));
		}
	}
	
	
	public static function destroy($iddrink){
		self::check_logged_in();

		$drink = new Drink(array('iddrink' => $iddrink));
		$drink->destroy();

		Redirect::to('/drink', array('message' => 'Drink has been removed successfully!'));
	}
}