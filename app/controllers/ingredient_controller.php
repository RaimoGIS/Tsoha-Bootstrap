<?php


class IngredientController extends BaseController{
	
	public static function index() {
		self::check_logged_in();

		$ingredient = Ingredient::all();
		View::make('ingredient/ingredient_list.html', array('ingredient' => $ingredient));
	}


	// this now called from DrinksController and it gets the ingredients for a specific drink. Refactor!
	public static function show() {
		self::check_logged_in();

		$ingredients = Ingredient::all();
		return $ingredients;
	}

	// this method is used when new ingredient is created. Refactor together with show()
	public static function show_new($ingrid) {
		self::check_logged_in();

		$ingredient = Ingredient::find($ingrid);
		View::make('ingredient/ingredient_show.html', array('ingredient' => $ingredient));
	}


	public static function create() {
		self::check_logged_in();

		$ingredients = Ingredient::all();
		View::make('ingredient/ingredient_new.html');

	}

	public static function store(){
		self::check_logged_in();
		$params = $_POST;
		
		$attributes = array(
			'name' => $params['ingredientname'],
			'description' => $params['description']
		);
		
		$ingredient = new Ingredient($attributes);

		// add error checking here
		$errors = $ingredient->errors();

		if(count($errors) > 0){
			View::make('ingredient/new.html', array('errors' => $errors, 'attributes' => $attributes));
		}else{
			$ingredient->save();
			
			Redirect::to('/ingredient/' . $ingredient->ingrid, array('message' => 'Ingredient has been added to your library!'));	
		}

	}

}
