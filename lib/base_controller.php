<?php

  class BaseController{

    public static function get_user_logged_in(){
      // Get user by user_id
        if(isset($_SESSION['user'])){
          $user_id = $_SESSION['user'];
          $user = User::find($user_id);

          return $user;
        }
      return null;
    }

    public static function check_logged_in(){
      // Check that user has logged in. If not redirect to logging in page
      if(!isset($_SESSION['user'])){
        Redirect::to('/login', array('message' => 'You need to login first!'));
      }
    }
  }
