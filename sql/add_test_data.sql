-- Lisää INSERT INTO lauseet tähän tiedostoon

INSERT INTO DrinkUser (name, password, modifyRight, adminRight) VALUES ('RaimoAdmin', '1234', 'true', 'true');
INSERT INTO DrinkUser (name, password, modifyRight, adminRight) VALUES ('rekisteröitynyt', '1234', 'true', 'false');

INSERT INTO Ingredient (name, description) VALUES ('cachaca','strong spirit made out of sugarcane');
INSERT INTO Ingredient (name, description) VALUES ('icecube','H20');
INSERT INTO Ingredient (name, description) VALUES ('lime wedge','fruit');
INSERT INTO Ingredient (name, description) VALUES ('vodka','strong spirit usually out ot barley');
INSERT INTO Ingredient (name, description) VALUES ('martini','liquer');

INSERT INTO DrinkTypes (name) VALUES ('Shot');
INSERT INTO DrinkTypes (name) VALUES ('Aperative');

INSERT INTO Drink (drinkName, preparation) VALUES ('caipirinha', 'murskaa jäät ja lisää viina sekä lime') 

