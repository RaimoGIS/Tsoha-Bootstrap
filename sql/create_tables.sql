-- Lisää CREATE TABLE lauseet tähän tiedostoon
BEGIN;
	CREATE TABLE DrinkUser (
		idUser SERIAL NOT NULL PRIMARY KEY,
		name varchar(50) NOT NULL,
		password varchar(50) NOT NULL,
		modifyRight BOOLEAN NOT NULL DEFAULT 'false',
		adminRight BOOLEAN NOT NULL DEFAULT 'false',
		savedDrinks INTEGER[]
	);

	CREATE TABLE Ingredient (
		ingrId SERIAL PRIMARY KEY NOT NULL,
		name varchar(50) NOT NULL,
		description varchar(400)
	);

	CREATE TABLE DrinkTypes (
		idDrinkType SERIAL PRIMARY KEY,
		name varchar(30) NOT NULL
	);

	CREATE TABLE Drink (
		idDrink SERIAL PRIMARY KEY,
		createdBy INTEGER REFERENCES DrinkUser(idUser),
		createdAt DATE,
		drinkName varchar(50) NOT NULL,
		preparation text NOT NULL,
		accepted boolean NOT NULL DEFAULT 'false',
		nonAlcoholic BOOLEAN,
		drinkType INTEGER REFERENCES DrinkTypes (idDrinkType),
		description varchar(400)
	);

	CREATE TABLE DrinkIngredients (
		drink INTEGER NOT NULL,
		ingredient INTEGER NOT NULL,
		amount INTEGER,
		FOREIGN KEY(drink) REFERENCES Drink(idDrink) ON DELETE CASCADE,
		FOREIGN KEY(ingredient) REFERENCES Ingredient(ingrId) ON DELETE CASCADE
	);
COMMIT;

